//Create anew server that handles requests for user routes. The following features must be included:

/*
	1. Get a list of all users. 
	2. View a specific user's profile.
	3. Update user information. 
	4. Delete a user. 
	5. Register a new user.
*/ 

//Create a new Postman collection called "Node.js Activity 1" and add all the routes to that collection. Make sure to test each one, and then export the collection to your a1 folder.


let http = require("http")

const server = http.createServer(function(request, response){

	if(request.url === '/api/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Users retrieved from database.")

	}else if(request.url === '/api/users/123' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 retrieved from database.")

	}else if(request.url === '/api/users/123' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 updated.")	

	}else if(request.url === '/api/users/123' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User 123 deleted.")	
			
	}else if(request.url === '/api/users' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("New user added to database.")
				
	}
})

const port = 3000

server.listen(port)

console.log(`Server running at port ${port}`)